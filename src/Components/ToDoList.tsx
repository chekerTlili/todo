import { VStack,StackDivider, Badge } from '@chakra-ui/react'
import React from 'react'
import { Todo } from '../Model'
import SingleTodo from './SingleTodo';


interface Props{
  todos:Todo[];
  setTodos:React.Dispatch<React.SetStateAction<Todo[]>>;
}


function todolist({todos ,setTodos}:Props) {

if(!todos.length){
  return <Badge
  borderColor="green.600"
  borderWidth="2px"
  p="4"
  borderRadius="lg"
  w='20%'
  textColor="green.600"
  textAlign="center"
  fontSize="lg">
        No Todos for today
  </Badge>
}else{

  return (
    <VStack 
      divider={<StackDivider/>}
      borderColor="gray.200"
      borderWidth="2px"
      p="4"
      borderRadius="lg"
      w='30%'
     // maxW={{base :'90vw', sm :'80vw' , lg :"50" , xl :''}}
      alignItems='stretch'
      mt={'10'}
    >
    
      {todos.map(todo =>(
        <SingleTodo todo={todo} key={todo.id} todos={todos} setTodos={setTodos} />
        ))}

    </VStack>
  )}
}

export default todolist;