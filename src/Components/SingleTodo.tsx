import { HStack, Spacer ,Text, IconButton, Input} from '@chakra-ui/react'
import React, { useEffect, useRef, useState } from 'react'
import { FaEdit, FaTrash } from 'react-icons/fa'
import { Todo } from '../Model';


interface Props{
    todo:Todo;
    todos:Todo[];
    setTodos:React.Dispatch<React.SetStateAction<Todo[]>>;
     
  }

  
const SingleTodo = ({todos, todo ,setTodos}:Props) => {

  const [edit , setEdit] = useState <boolean>(false);
  const [editTodo , setEditTodo] = useState <string>(todo.todo);

  const deleteTodo = (id :number) =>{
    setTodos(todos.filter((todo) => todo.id !== id));
} ;

const handleEdit = (e : React.FormEvent, id : number) =>{
  e.preventDefault();

  setTodos(
    todos.map((todo) => (todo.id === id ? { ...todo, todo: editTodo } : todo))
  );
  setEdit(false);
}

const inputref = useRef<HTMLInputElement>(null);
useEffect(() =>{
  inputref.current?.focus();},
  [edit]
);

  return (
    <form onSubmit={(e)=>{
      handleEdit(e , todo.id);
      ;
    }}>
    <HStack key={todo.id}>

      {
        edit?(
                <Input value={editTodo} onChange={(e) => {setEditTodo(e.target.value)}}></Input>
        ):(
                <Text>{todo.todo}</Text>
          )
      }
            <Spacer/>
            <IconButton aria-label={'edit'} icon={<FaEdit/>} 
            onClick = {()=>{
              if (!edit)
              {setEdit(!edit);}
            }}
               isRound={true}></IconButton>
            <IconButton 
                aria-label={'delete'} 
                icon={<FaTrash/> }
                onClick ={()=>deleteTodo(todo.id)}isRound={true}>

            </IconButton>
            
        </HStack>
        </form>
  )
}

export default SingleTodo;