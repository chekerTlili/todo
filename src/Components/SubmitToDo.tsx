import { Button, VStack,Input, useToast } from '@chakra-ui/react'
import React, { useRef } from 'react'



interface Props{
    todo:string;
    setTodo:React.Dispatch<React.SetStateAction<string>>;
    handleTodo :(e: React.FormEvent)=>void;
}

const SubmitTodo = ({todo,setTodo,handleTodo}:Props) => {

  const inputRef= useRef<HTMLInputElement>(null);

  

  return (
    <form onSubmit={(e)=>{
      handleTodo(e);
      inputRef.current?.blur();
    }}>
        <VStack mt='8'>
            <Input 
                w="100"
                ref={inputRef}
                variant='filled' 
                placeholder='Enter your ToDo...'
                value={todo}
                onChange={
                  (e)=>setTodo(e.target.value)
                }>
                
                </Input>
            <Button colorScheme='teal' variant='solid' type='submit'>Add ToDo</Button>
        </VStack>
        
    </form>
  )
}

export default SubmitTodo;