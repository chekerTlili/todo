import * as React from "react"
import { VStack , Heading, IconButton, useToast,useColorMode} from "@chakra-ui/react"
import Todolist from "./Components/ToDoList"
import SubmitTodo from "./Components/SubmitToDo"
import { useEffect, useState } from "react"
import { Todo } from "./Model"
import { FaMoon, FaSun } from "react-icons/fa"



 function App ()  {
  

    const [todo,setTodo]=useState<string>('');
   
    const [todos,setTodos] = useState <Todo[]>(() =>JSON.parse(String (localStorage.getItem('todos')))||[]);

    useEffect(() => { 
      localStorage.setItem('todos', JSON.stringify(todos));
      
    }, [todos]);
    const toast = useToast();

    const handelTodo=(e : React.FormEvent)=>{
        e.preventDefault();
      
        if(todo && todo.length!==0){
        setTodos([...todos,{id: Date.now(),todo:todo}]);
        setTodo("");
        }else{  toast({
          title: 'To Do empty',
          description: "You didn't add any To Do .",
          status: 'error',
          duration: 9000,
          isClosable: true,
        });
        return}
    }

    const{colorMode ,toggleColorMode} =useColorMode();
  
  return(
    <VStack p={4}>
        <IconButton 
            aria-label={" mode"} 
            icon={colorMode==="light"?<FaMoon/> :<FaSun/> }
            size="lg"
            isRound={true}
            alignSelf="flex-end"
            onClick={toggleColorMode}></IconButton>
        <Heading
          mb='10' 
          bgGradient="linear(to-r,pink.500  ,blue.600)" 
          bgClip='text'>
            ToDo App
        </Heading>
        <Todolist todos={todos} setTodos={setTodos} ></Todolist>
        <SubmitTodo todo={todo} setTodo={setTodo} handleTodo={handelTodo}></SubmitTodo>
        
        
    </VStack>)
    
  

 }
export default App;
